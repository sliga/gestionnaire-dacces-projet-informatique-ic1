/**
 * 
 */
package dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;

import business.Lieu;

/**
 * @author ASUS
 *
 */

public class LieuDAO extends Dao<Lieu> {
	
	private String ld = "LieuDAO";
	@Override
	public Lieu find(int id) {
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "SELECT * FROM  Lieu WHERE id_lieu= ?";
			
			rs= Dao.getRowSet();
			rs.setCommand(sql);
			rs.setInt(1, id);
			rs.execute();

			while(rs.next()){
				id = rs.getInt(1);
				String nom = rs.getString(2);
				String emplacement = rs.getString(3);
				String heureOuverture = rs.getString(4);
				String heureFermeture = rs.getString(5);
				boolean b = rs.getString(6).equals(true);
				object = new Lieu(id, nom, emplacement,heureOuverture, heureFermeture);
			}
		} catch (Exception e) {
			System.out.println(ld+".find() :Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return object;
	}

	@Override
	public boolean create(Lieu object) {
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "INSERT INTO Lieu (id_lieu, nom_lieu, emplacement, heure_ouverture, heure_fermeture) VALUES (?,?,?,?,?)";
			
			rs = Dao.getRowSet();
			rs.setCommand(sql);
			rs.setInt(1, object.getIdLieu());
			rs.setString(2, object.getNomLieu());
			rs.setString(3, object.getEmplacement());
			rs.setString(4, object.getHeureOuverture());
			rs.setString(5, object.getHeureFermeture());
			rs.execute();
			executeStatus = true;
			
		} catch (Exception e) {
			System.out.println(ld+".create() : Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return executeStatus;
		
	}

	@Override
	public boolean update(Lieu object) {
		
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "UPDATE Lieu SET id_lieu = ?, nom_lieu =?, emplacement = ?, heure_ouverture = ?, heure_fermeture =? WHERE id_lieu = ?";

			rs = Dao.getRowSet();
			rs.setCommand(sql);
			rs.setInt(1, object.getIdLieu());
			rs.setString(2, object.getNomLieu());
			rs.setString(3, object.getEmplacement());
			rs.setString(4, object.getHeureOuverture());
			rs.setString(5, object.getHeureFermeture());
			rs.setInt(6, object.getIdLieu());
			rs.execute();
			executeStatus = true;
			
		} catch (Exception e) {
			System.out.println(ld+".update () : Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return executeStatus;
	}

	@Override
	public boolean delete(Lieu object) {
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "DELETE FROM Lieu WHERE id_lieu =?";

			rs = Dao.getRowSet();
			rs.setCommand(sql);
			rs.setInt(1, object.getIdLieu());
			rs.execute();
			executeStatus = rs.rowDeleted();
		} catch (Exception e) {
			System.out.println(ld+".delete () : Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return executeStatus;
		
	}

	@Override
	public List<Lieu> getList() {
		List<Lieu> listeLieu = new ArrayList<>();
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "SELECT * FROM  Lieu";
			
			rs= Dao.getRowSet();
			rs.setCommand(sql);
			rs.execute();

			while(rs.next()){
				int id = rs.getInt(1);
				String nom = rs.getString(2);
				String emplacement = rs.getString(3);
				String heureOuverture = rs.getString(4);
				String heureFermeture = rs.getString(5);
				boolean b = rs.getString(6).equals(true);
				object = new Lieu(id, nom, emplacement,heureOuverture, heureFermeture);
				listeLieu.add(object);
			}
		} catch (Exception e) {
			System.out.println(ld+".getList() : Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return listeLieu;
	}

}
