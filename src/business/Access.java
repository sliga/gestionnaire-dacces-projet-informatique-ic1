/**
 * 
 */
package business;

/**
 * @author ASUS
 *
 */
public class Access {
	/**
	 * 
	 */
	private int idAcces;
	private String typeAcces;
	private int idLieuAssocie;
	
	public Access(int idAcces, String typeAcces, int idLieuAssocie) {
		this.idAcces = idAcces;
		this.typeAcces = typeAcces;
	}
	/**
	 * @return the lieuAssocie
	 */
	public int getLieuAssocie() {
		return idLieuAssocie;
	}
	/**
	 * @param lieuAssocie the lieuAssocie to set
	 */
	public void setLieuAssocie(int lieuAssocie) {
		this.idLieuAssocie = lieuAssocie;
	}
	/**
	 * @return the idAcces
	 */
	public int getIdAcces() {
		return idAcces;
	}
	/**
	 * @param idAcces the idAcces to set
	 */
	public void setIdAcces(int idAcces) {
		this.idAcces = idAcces;
	}
	/**
	 * @return the typeAcces
	 */
	public String getTypeAcces() {
		return typeAcces;
	}
	/**
	 * @param typeAcces the typeAcces to set
	 */
	public void setTypeAcces(String typeAcces) {
		this.typeAcces = typeAcces;
	}
	
}
