package dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;

import business.Badge;

public class BadgeDAO extends Dao<Badge> {
	String bd ="BadgeDAO";
	@Override
	public Badge find(int id) {
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "SELECT * FROM  Badge WHERE id_Badge= ?";
			
			rs= Dao.getRowSet();
			rs.setCommand(sql);
			rs.setInt(1, id);
			rs.execute();
	
			while(rs.next()){
				id = rs.getInt(1);
				int idPersonneAssocie = rs.getInt(3);
				object = new Badge(id, idPersonneAssocie );
				System.out.println(id+""+idPersonneAssocie);
			}
		} catch (Exception e) {
			System.out.println(bd+".find() :Impossible d'acc�der aux infos");
			e.printStackTrace();
			e.getMessage();
		}
		return object;
	}

	@Override
	public boolean create(Badge object) {
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "INSERT INTO Badge (id_Badge, id_personne_associe) VALUES (?,?)";
			
			rs = Dao.getRowSet();
			rs.setCommand(sql);
			rs.setInt(1, object.getIdBadge());
			rs.setInt(2, object.getPersonneAssocie());
			rs.execute();
			executeStatus = true;
			
		} catch (Exception e) {
			System.out.println(bd+".create() : Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return executeStatus;
		
	}

	@Override
	public boolean update(Badge object) {
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "UPDATE Badge SET id_Badge = ?, id_personne_associe = ? WHERE id_badge = ?";

			rs = Dao.getRowSet();
			rs.setCommand(sql);
			rs.setInt(1, object.getIdBadge());
			rs.setInt(2, object.getPersonneAssocie());
			rs.setInt(3, object.getIdBadge());
			rs.execute();
			executeStatus = true;
		} catch (Exception e) {
			System.out.println(bd+".update () : Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return executeStatus;
		
	}

	@Override
	public boolean delete(Badge object) {
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "DELETE FROM Badge WHERE id_Badge =?";

			rs = Dao.getRowSet();
			rs.setCommand(sql);
			rs.setInt(1, object.getIdBadge());
			rs.execute();
			executeStatus = true;
		} catch (Exception e) {
			System.out.println(bd+".delete () : Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return executeStatus;
		
	}

	@Override
	public List<Badge> getList() {
		List<Badge> listeBadge = new ArrayList<>();
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "SELECT * FROM  Badge";
			
			rs= Dao.getRowSet();
			rs.setCommand(sql);
			rs.execute();
	
			while(rs.next()){
				int id = rs.getInt(1);
				int idPersonneAssocie = rs.getInt(3);
				object = new Badge(id, idPersonneAssocie );
				listeBadge.add(object);
				System.out.println(id+""+idPersonneAssocie);
			}
		} catch (Exception e) {
			System.out.println(bd+".find() :Impossible d'acc�der aux infos");
			e.printStackTrace();
			e.getMessage();
		}
		return listeBadge;
	}

}
