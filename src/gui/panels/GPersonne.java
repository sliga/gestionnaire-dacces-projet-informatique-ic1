package gui.panels;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;

import business.Access;
import business.Personne;
import dao.Dao;
import dao.PersonneDAO;
import gui.events.ButtonAction;

public class GPersonne extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textFieldNom;
	private JTextField textFieldPrenom;
	private JTextField textFieldFonction;
	private JTextField textFieldBadge;
	private JTable table;
	private JTable tablePersonne;
	private JButton btnUpdate;
	private JButton btnCreate;
	private JButton btnDelete;
	private JButton btnSearch;
	private JComboBox comboYear;
	private JComboBox comboMonth;
	private JComboBox comboDay;

	private int id_personne;
	private String nom;
	private String prenom;
	private String fonction;
	private String date_naissance;
	private int id_badge_associe;
	private Personne person;
	private PersonneDAO pd;

	/**
	 * Create the panel.
	 */
	public GPersonne() {
		pd = new PersonneDAO();
		initGPersone();
		createEvents();
	}

	/**
	 * Read the input values from the input components as textField...
	 */
	public void inputValues() {
		ComboItems comboItem = new ComboItems();
		id_personne = pd.autoIncrementId("Personne");
		// id_badge_associe = Integer.parseInt(textFieldBadge.getText());
		date_naissance = comboItem.getDate();
		nom = textFieldNom.getText();
		prenom = textFieldPrenom.getText();
		fonction = textFieldFonction.getText();
		person = new Personne(id_personne, nom, prenom, fonction, date_naissance);
	}

	private void createEvents() {
		ButtonAction.crudEvents(btnUpdate, btnCreate, btnDelete, btnSearch);

		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inputValues();
				pd.update(person);
			}
		});

		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inputValues();
				id_personne = pd.autoIncrementId("personne");
				person.setIdPersonne(id_personne++);
				pd.create(person);
				if (((Dao<Personne>) pd).isExecuteStatus())
					pd.showValidExectutionMessage("create", "personne");
			}
		});

		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inputValues();
				pd.delete(person);
			}
		});

		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inputValues();
				pd.find(person.getIdPersonne());
			}
		});

	}

	@SuppressWarnings({ "unchecked", "rawtypes", "serial" })
	private void initGPersone() {
		JDesktopPane desktopPane = new JDesktopPane();
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(desktopPane,
				Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 844, Short.MAX_VALUE));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(desktopPane,
				Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 486, Short.MAX_VALUE));

		JLabel labelUserIcon = new JLabel("");
		labelUserIcon.setIcon(new ImageIcon(GPersonne.class.getResource("/resources/user_group.png")));

		JLabel lblNom = new JLabel("Nom");
		lblNom.setForeground(SystemColor.text);
		lblNom.setFont(new Font("Trebuchet MS", Font.BOLD, 22));

		JLabel lblPrenom = new JLabel("Pr\u00E9nom (s) : ");
		lblPrenom.setForeground(Color.WHITE);
		lblPrenom.setFont(new Font("Trebuchet MS", Font.BOLD, 22));

		JLabel lblDateDeNaissance = new JLabel("Date de naissance");
		lblDateDeNaissance.setForeground(Color.WHITE);
		lblDateDeNaissance.setFont(new Font("Trebuchet MS", Font.BOLD, 22));

		JLabel lblRfDuBadge = new JLabel("R\u00E9f du Badge");
		lblRfDuBadge.setForeground(Color.WHITE);
		lblRfDuBadge.setFont(new Font("Trebuchet MS", Font.BOLD, 22));

		textFieldNom = new JTextField();
		textFieldNom.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		textFieldNom.setColumns(10);

		textFieldPrenom = new JTextField();
		textFieldPrenom.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		textFieldPrenom.setColumns(10);

		comboDay = new JComboBox();
		comboDay.setModel(new DefaultComboBoxModel(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09",
				"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26",
				"27", "28", "29", "30", "31" }));
		comboDay.setToolTipText("Day");
		comboDay.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		comboMonth = new JComboBox();
		comboMonth.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));
		comboMonth.setModel(new DefaultComboBoxModel(
				new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));
		comboMonth.setToolTipText("Month");

		comboYear = new JComboBox();
		comboYear.setModel(new DefaultComboBoxModel(new String[] { "2010", "2009", "2008", "2007", "2006", "2005",
				"2004", "2003", "2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995", "1994", "1993", "1992",
				"1991", "1990", "1989", "1989", "1988", "1987", "1986", "1985", "1984", "1983", "1982", "1981", "1980",
				"1979", "1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971", "1970", "1969", "1968", "1967",
				"1966", "1965", "1964", "1963", "1962", "1961", "1960", "1959", "1958", "1957", "1956", "1955", "1954",
				"1953", "1952", "1951", "1950" }));
		comboYear.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));
		comboYear.setToolTipText("Year");

		textFieldFonction = new JTextField();
		textFieldFonction.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		textFieldFonction.setColumns(10);

		JLabel lblFonction = new JLabel("Fonction");
		lblFonction.setForeground(Color.WHITE);
		lblFonction.setFont(new Font("Trebuchet MS", Font.BOLD, 22));

		textFieldBadge = new JTextField();
		textFieldBadge.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		textFieldBadge.setColumns(10);

		btnUpdate = new JButton("Update");
		btnUpdate.setIcon(new ImageIcon(GPersonne.class.getResource("/resources/ok.png")));
		btnUpdate.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		btnCreate = new JButton("Create");
		btnCreate.setIcon(new ImageIcon(GPersonne.class.getResource("/resources/create.png")));
		btnCreate.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		btnDelete = new JButton("Delete");
		btnDelete.setIcon(new ImageIcon(GPersonne.class.getResource("/resources/delete.png")));
		btnDelete.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		btnSearch = new JButton("Search");
		btnSearch.setIcon(new ImageIcon(GPersonne.class.getResource("/resources/search.png")));
		btnSearch.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		table = new JTable();

		tablePersonne = new JTable();
		tablePersonne.setFillsViewportHeight(true);
		tablePersonne.setShowGrid(false);
		tablePersonne.setToolTipText("Nom\r\nPr\u00E9noms\r\n");
		tablePersonne.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tablePersonne.setModel(new DefaultTableModel(
				new Object[][] { { null, null, null, null, null, null }, { null, null, null, null, null, null },
						{ null, null, null, null, null, null }, { null, null, null, null, null, null },
						{ null, null, null, null, null, null }, { null, null, null, null, null, null },
						{ null, null, null, null, null, null }, { null, null, null, null, null, null },
						{ null, null, null, null, null, null }, { null, null, null, null, null, null }, },
				new String[] { "Identifiers", "Nom", "Pr\u00E9nom(s)", "Date de naissance", "Fonction",
						"R\u00E9f. du Badge" }) {
			Class[] columnTypes = new Class[] { Integer.class, String.class, String.class, String.class, String.class,
					Integer.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		tablePersonne.getColumnModel().getColumn(0).setPreferredWidth(65);
		tablePersonne.getColumnModel().getColumn(1).setPreferredWidth(137);
		tablePersonne.getColumnModel().getColumn(2).setPreferredWidth(159);
		tablePersonne.getColumnModel().getColumn(2).setMinWidth(36);
		tablePersonne.getColumnModel().getColumn(3).setPreferredWidth(117);
		tablePersonne.getColumnModel().getColumn(4).setPreferredWidth(108);
		tablePersonne.getColumnModel().getColumn(5).setPreferredWidth(94);
		tablePersonne.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		tablePersonne.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
		tablePersonne.setCellSelectionEnabled(true);
		tablePersonne.setColumnSelectionAllowed(true);
		GroupLayout gl_desktopPane = new GroupLayout(desktopPane);
		gl_desktopPane.setHorizontalGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING).addGroup(gl_desktopPane
				.createSequentialGroup()
				.addGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_desktopPane.createSequentialGroup()
								.addGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_desktopPane.createSequentialGroup().addGap(268).addComponent(
												lblNom, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_desktopPane.createSequentialGroup().addGap(19).addComponent(labelUserIcon)
										.addGap(93)
										.addGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING)
												.addComponent(lblRfDuBadge, GroupLayout.PREFERRED_SIZE, 187,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(lblDateDeNaissance).addComponent(lblPrenom)
												.addComponent(lblFonction, GroupLayout.PREFERRED_SIZE, 187,
														GroupLayout.PREFERRED_SIZE))))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_desktopPane.createSequentialGroup().addGap(21)
												.addGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING)
														.addComponent(textFieldPrenom, GroupLayout.PREFERRED_SIZE, 278,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(textFieldNom, GroupLayout.PREFERRED_SIZE, 278,
																GroupLayout.PREFERRED_SIZE)
														.addGroup(gl_desktopPane.createSequentialGroup()
																.addComponent(comboDay, GroupLayout.PREFERRED_SIZE, 75,
																		GroupLayout.PREFERRED_SIZE)
																.addGap(18)
																.addComponent(comboMonth, GroupLayout.PREFERRED_SIZE,
																		76, GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(ComponentPlacement.UNRELATED)
																.addComponent(comboYear, GroupLayout.PREFERRED_SIZE, 93,
																		GroupLayout.PREFERRED_SIZE))))
										.addGroup(gl_desktopPane.createSequentialGroup().addGap(18)
												.addGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING)
														.addComponent(textFieldBadge, GroupLayout.PREFERRED_SIZE, 278,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(textFieldFonction, GroupLayout.PREFERRED_SIZE,
																278, GroupLayout.PREFERRED_SIZE)))))
						.addGroup(gl_desktopPane.createSequentialGroup()
								.addGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_desktopPane.createSequentialGroup().addGap(213)
												.addComponent(btnUpdate).addPreferredGap(ComponentPlacement.UNRELATED)
												.addComponent(btnCreate, GroupLayout.PREFERRED_SIZE, 135,
														GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(btnDelete, GroupLayout.PREFERRED_SIZE, 135,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED).addComponent(btnSearch,
												GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_desktopPane.createSequentialGroup().addGap(45).addComponent(tablePersonne,
										GroupLayout.DEFAULT_SIZE, 744, Short.MAX_VALUE)))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(table, GroupLayout.PREFERRED_SIZE, 1, GroupLayout.PREFERRED_SIZE)))
				.addContainerGap(48, Short.MAX_VALUE)));
		gl_desktopPane.setVerticalGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_desktopPane.createSequentialGroup()
						.addGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_desktopPane.createSequentialGroup().addGap(106).addComponent(labelUserIcon,
										GroupLayout.PREFERRED_SIZE, 179, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_desktopPane.createSequentialGroup().addGap(58)
								.addGroup(gl_desktopPane.createParallelGroup(Alignment.BASELINE).addComponent(lblNom,
										GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
								.addComponent(textFieldNom, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING).addGroup(gl_desktopPane
										.createSequentialGroup()
										.addComponent(lblPrenom, GroupLayout.PREFERRED_SIZE, 34,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addGroup(gl_desktopPane.createParallelGroup(Alignment.BASELINE)
												.addComponent(lblDateDeNaissance, GroupLayout.PREFERRED_SIZE, 34,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(comboDay, GroupLayout.PREFERRED_SIZE, 36,
														GroupLayout.PREFERRED_SIZE)))
										.addGroup(gl_desktopPane.createSequentialGroup()
												.addComponent(textFieldPrenom, GroupLayout.PREFERRED_SIZE, 34,
														GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.UNRELATED)
												.addGroup(gl_desktopPane.createParallelGroup(Alignment.BASELINE)
														.addComponent(comboMonth, GroupLayout.PREFERRED_SIZE, 36,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(comboYear, GroupLayout.PREFERRED_SIZE, 36,
																GroupLayout.PREFERRED_SIZE))))
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING)
										.addComponent(textFieldFonction, GroupLayout.PREFERRED_SIZE, 34,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(lblFonction, GroupLayout.PREFERRED_SIZE, 34,
												GroupLayout.PREFERRED_SIZE))
								.addGap(19)
								.addGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING)
										.addComponent(textFieldBadge, GroupLayout.PREFERRED_SIZE, 34,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(lblRfDuBadge, GroupLayout.PREFERRED_SIZE, 34,
												GroupLayout.PREFERRED_SIZE))))
				.addGap(50)
				.addGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING).addComponent(btnUpdate)
						.addComponent(btnCreate, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnDelete, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSearch, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_desktopPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_desktopPane.createSequentialGroup().addGap(119).addComponent(table,
								GroupLayout.PREFERRED_SIZE, 1, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_desktopPane.createSequentialGroup().addGap(18).addComponent(tablePersonne,
								GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE)))
				.addContainerGap(116, Short.MAX_VALUE)));
		desktopPane.setLayout(gl_desktopPane);
		setLayout(groupLayout);

	}

	class ComboItems implements ActionListener {
		private String dayItem;
		private String monthItem;
		private String yearItem;
		private String date;

		public String getDate() {
			return date = dayItem + "/" + monthItem + "/" + yearItem;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			dayItem = (String) comboDay.getSelectedItem();
			monthItem = (String) comboMonth.getSelectedItem();
			yearItem = (String) comboYear.getSelectedItem();
		}

	}
}
