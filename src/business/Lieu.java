/**
 * 
 */
package business;

/**
 * @author ASUS
 *
 */
public class Lieu {
	
	private int idLieu;
	private String nomLieu;
	private String emplacement;
	private String heureOuverture;
	private String heureFermeture;
	public Lieu(int idLieu, String nomLieu, String emplacement, String heureOverture, String heureFermeture) {
		this.idLieu = idLieu;
		this.nomLieu = nomLieu;
		this.emplacement = emplacement;
		this.heureOuverture = heureOverture;
		this.heureFermeture = heureFermeture;
	}
	/**
	 * @return the idLieu
	 */
	public int getIdLieu() {
		return idLieu;
	}
	/**
	 * @param idLieu the idLieu to set
	 */
	public void setIdLieu(int idLieu) {
		this.idLieu = idLieu;
	}
	/**
	 * @return the nomLieu
	 */
	public String getNomLieu() {
		return nomLieu;
	}
	/**
	 * @param nomLieu the nomLieu to set
	 */
	public void setNomLieu(String nomLieu) {
		this.nomLieu = nomLieu;
	}
	/**
	 * @return the emplacement
	 */
	public String getEmplacement() {
		return emplacement;
	}
	/**
	 * @param emplacement the emplacement to set
	 */
	public void setEmplacement(String emplacement) {
		this.emplacement = emplacement;
	}
	/**
	 * @return the heureOuverture
	 */
	public String getHeureOuverture() {
		return heureOuverture;
	}
	/**
	 * @param heureOuverture the heureOuverture to set
	 */
	public void setHeureOuverture(String heureOuverture) {
		this.heureOuverture = heureOuverture;
	}
	/**
	 * @return the heureFermeture
	 */
	public String getHeureFermeture() {
		return heureFermeture;
	}
	/**
	 * @param heureFermeture the heureFermeture to set
	 */
	public void setHeureFermeture(String heureFermeture) {
		this.heureFermeture = heureFermeture;
	}
}
