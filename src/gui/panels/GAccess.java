package gui.panels;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

import gui.events.ButtonAction;
import javax.swing.table.DefaultTableModel;

import business.Access;
import dao.AccessDAO;

import javax.swing.JScrollPane;

public class GAccess extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textFieldRefBadge;
	private JTextField textFieldLieuAssocie;
	private JButton btnUpdate;
	private JButton btnCreate;
	private JButton btnDelete;
	private JButton btnSearch;
	private JComboBox comboTypeAccess;

	private int id_access;
	private String type_access;
	private int id_lieu_associe;
	private Access access;
	private AccessDAO ad;

	/**
	 * Create the panel.
	 */
	public GAccess() {
		ad = new AccessDAO();
		initGAccess();
		createEvents();
	}

	/**
	 * Read the input values from the input components as textField...
	 */
	public void inputValues() {
		ComboItemState comboItem = new ComboItemState();
		id_access = Integer.parseInt(textFieldRefBadge.getText());
		type_access = comboItem.getItemSelected();
		id_lieu_associe = Integer.parseInt(textFieldLieuAssocie.getText());
		access = new Access(id_access, type_access, id_lieu_associe);
	}

	private void createEvents() {
		ButtonAction.crudEvents(btnUpdate, btnCreate, btnDelete, btnSearch);

		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inputValues();
				ad.update(access);
			}
		});

		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inputValues();
				ad.create(access);
			}
		});

		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inputValues();
				ad.delete(access);
			}
		});

		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inputValues();
				access = ad.find(access.getIdAcces());
			}
		});
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initGAccess() {
		setLayout(null);

		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBounds(0, 0, 881, 774);
		add(desktopPane);

		JLabel lblicon = new JLabel("");
		lblicon.setIcon(new ImageIcon(GAccess.class.getResource("/resources/locker_icon.png")));
		lblicon.setBounds(146, 49, 156, 179);

		JLabel lblIdLieuxAssoci = new JLabel("ID Lieux associ\u00E9");
		lblIdLieuxAssoci.setBounds(356, 162, 180, 34);
		lblIdLieuxAssoci.setForeground(Color.WHITE);
		lblIdLieuxAssoci.setFont(new Font("Trebuchet MS", Font.BOLD, 22));

		JLabel lblType = new JLabel("Type");
		lblType.setBounds(468, 104, 68, 34);
		lblType.setForeground(Color.WHITE);
		lblType.setFont(new Font("Trebuchet MS", Font.BOLD, 22));

		JLabel lblRfAccess = new JLabel("R\u00E9f. Access");
		lblRfAccess.setBounds(409, 58, 131, 34);
		lblRfAccess.setForeground(Color.WHITE);
		lblRfAccess.setFont(new Font("Trebuchet MS", Font.BOLD, 22));

		textFieldRefBadge = new JTextField();
		textFieldRefBadge.setBounds(545, 49, 278, 43);
		textFieldRefBadge.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		textFieldRefBadge.setColumns(10);

		textFieldLieuAssocie = new JTextField();
		textFieldLieuAssocie.setBounds(545, 158, 278, 43);
		textFieldLieuAssocie.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		textFieldLieuAssocie.setColumns(10);

		btnUpdate = new JButton("Update");

		btnUpdate.setIcon(new ImageIcon(GAccess.class.getResource("/resources/ok.png")));
		btnUpdate.setBounds(207, 270, 143, 38);
		btnUpdate.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		btnCreate = new JButton("Create");

		btnCreate.setIcon(new ImageIcon(GAccess.class.getResource("/resources/create.png")));
		btnCreate.setBounds(377, 270, 135, 38);
		btnCreate.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		btnDelete = new JButton("Delete");

		btnDelete.setIcon(new ImageIcon(GAccess.class.getResource("/resources/delete.png")));
		btnDelete.setBounds(530, 270, 135, 38);
		btnDelete.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		btnSearch = new JButton("Search");

		btnSearch.setIcon(new ImageIcon(GAccess.class.getResource("/resources/search.png")));
		btnSearch.setBounds(683, 270, 135, 38);
		btnSearch.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));
		desktopPane.setLayout(null);
		desktopPane.add(lblicon);
		desktopPane.add(lblIdLieuxAssoci);
		desktopPane.add(lblType);
		desktopPane.add(lblRfAccess);
		desktopPane.add(textFieldRefBadge);
		desktopPane.add(textFieldLieuAssocie);
		desktopPane.add(btnUpdate);
		desktopPane.add(btnCreate);
		desktopPane.add(btnDelete);
		desktopPane.add(btnSearch);

		comboTypeAccess = new JComboBox();
		comboTypeAccess.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));
		comboTypeAccess.setEditable(true);
		comboTypeAccess
				.setModel(new DefaultComboBoxModel(new String[] { "Barri\u00E8re", "Parking", "Porte", "Grille" }));
		comboTypeAccess.setBounds(545, 102, 278, 44);
		comboTypeAccess.addActionListener(new ComboItemState());
		desktopPane.add(comboTypeAccess);

	}

	class ComboItemState implements ActionListener {
		private String itemSelected;

		@Override
		public void actionPerformed(ActionEvent e) {
			itemSelected = (String) comboTypeAccess.getSelectedItem();
		}

		public String getItemSelected() {
			return itemSelected;
		}

	}
}
