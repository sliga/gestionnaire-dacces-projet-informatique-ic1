package dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;

import business.Access;
import business.Personne;

public class AccessDAO extends Dao<Access> {
	
	String ad = "AccessDAO";
	@Override
	public Access find(int id) {
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "SELECT * FROM  Acces WHERE id_acces = ?";
			
			rs= Dao.getRowSet();
			rs.setCommand(sql);
			rs.setInt(1, id);
			rs.execute();
			
			while(rs.next()){
				id = rs.getInt(1);
				String typeAcces = rs.getString(2);
				int idLieuAssocie = rs.getInt(3);
				object = new Access(id, typeAcces, idLieuAssocie);
			}
		} catch (Exception e) {
			System.out.println(ad+".find() :Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return object;
	}

	@Override
	public boolean create(Access object) {
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "INSERT INTO Acces (id_acces, type_acces, lieu_associe) VALUES (?,?,?)";
			rs = Dao.getRowSet();
			rs.setCommand(sql);
			rs.setInt(1, object.getIdAcces());
			rs.setString(2, object.getTypeAcces());
			rs.setInt(3, object.getLieuAssocie());
			rs.execute();
			executeStatus = true;
		} catch (Exception e) {
			System.out.println(ad+".create() : Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return executeStatus;
	}

	@Override
	public boolean update(Access object) {
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "UPDATE Acces SET id_acces = ?, type_acces = ?, lieu_associe = ? WHERE id_acces = ?";
			System.out.println(object.getIdAcces());
			rs = Dao.getRowSet();
			rs.setCommand(sql);
			rs.setInt(1, object.getIdAcces());
			rs.setString(2, object.getTypeAcces());
			rs.setInt(3, object.getLieuAssocie());
			rs.setInt(4, object.getIdAcces());
			rs.execute();
			executeStatus = true;
			
		} catch (Exception e) {
			System.out.println(ad+".update () : Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return executeStatus;
	}

	@Override
	public boolean delete(Access object) {
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "DELETE FROM Acces WHERE id_acces =?";

			rs = Dao.getRowSet();
			rs.setCommand(sql);
			rs.setInt(1, object.getIdAcces());
			rs.execute();
			executeStatus = true;
			
		} catch (Exception e) {
			System.out.println(ad+".delete () : Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return executeStatus;
		
	}

	@Override
	public List<Access> getList() {
		List<Access> listeAccess = new ArrayList<>();
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "SELECT * FROM  Acces";
			
			rs= Dao.getRowSet();
			rs.setCommand(sql);
			rs.execute();
			
			while(rs.next()){
				int id = rs.getInt(1);
				String typeAcces = rs.getString(2);
				int idLieuAssocie = rs.getInt(3);
				object = new Access(id, typeAcces, idLieuAssocie);
				listeAccess.add(object);
				System.out.println(id+typeAcces+idLieuAssocie);
			}
		} catch (Exception e) {
			System.out.println(ad+".find() :Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return listeAccess;
	}

}
