package dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.RowSetEvent;
import javax.sql.RowSetListener;
import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;
import javax.swing.JOptionPane;

import business.Personne;

public class PersonneDAO extends Dao<Personne>{
	
	String className = "PersonneDAO";
	@Override
	public Personne find(int id) {
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "SELECT * FROM  Personne WHERE id_personne= ?";
			
			rs= Dao.getRowSet();
			rs.setCommand(sql);
			rs.setInt(1, id);
			rs.execute();

			while(rs.next()){
				id = rs.getInt(1);
				String nom = rs.getString(2);
				String prenom = rs.getString(3);
				String fonction = rs.getString(4);
				String date = rs.getString(5);
				object = new Personne(id, nom, prenom,fonction, date );
			}
		} catch (Exception e) {
			this.showErrorMessage("find() ", className);
			System.out.println(className+".find() :Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return object;
	}

		

	@Override
	public boolean create(Personne object) {
		
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "INSERT INTO Personne (id_personne, nom, prenom, fonction, date_naissance) VALUES (?,?,?,?,?)";
			
			rs = Dao.getRowSet();
			rs.setCommand(sql);
			rs.setInt(1, object.getIdPersonne());
			rs.setString(2, object.getNom());
			rs.setString(3, object.getPrenom());
			rs.setString(4, object.getFonction());
			rs.setString(5, object.getDateNaissance());
			rs.execute();
			executeStatus = true;
			
		} catch (Exception e) {
			this.showErrorMessage("create() ", className);
			System.out.println(className+".create() : Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return executeStatus;
	}

	@Override
	public boolean update(Personne object) {
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "UPDATE Personne SET id_personne = ?, nom = ?, prenom = ?, fonction = ?, date_naissance = ? WHERE id_personne = ?";

			rs = Dao.getRowSet();
			rs.setCommand(sql);
			rs.setInt(1, object.getIdPersonne());
			rs.setString(2, object.getNom());
			rs.setString(3, object.getPrenom());
			rs.setString(4, object.getFonction());
			rs.setString(5, object.getDateNaissance());
			rs.setInt(6, object.getIdPersonne());
			rs.execute();
			executeStatus = true;
			
		} catch (Exception e) {
			this.showErrorMessage("update() ", className);
			System.out.println(className+".update() : Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return executeStatus;
		
	}

	@Override
	public boolean delete(Personne object) {
		
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "DELETE FROM Personne WHERE id_personne =?";

			rs = Dao.getRowSet();
			rs.setCommand(sql);
			rs.setInt(1, object.getIdPersonne());
			rs.execute();
			executeStatus = true;
			
		} catch (Exception e) {
			this.showErrorMessage("delete() ", className);
			System.out.println(className+".delete() : Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return executeStatus;
	}



	@Override
	public List<Personne> getList() {
		List<Personne> listePersonne = new ArrayList<>();
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "SELECT * FROM  Personne";
			
			rs= Dao.getRowSet();
			rs.setCommand(sql);
			rs.execute();
			while(rs.next()){

				int id = rs.getInt(1);
				String nom = rs.getString(2);
				String prenom = rs.getString(3);
				String fonction = rs.getString(4);
				String date = rs.getString(5);
				object = new Personne(id, nom, prenom,fonction, date );
				listePersonne.add(object);
				System.out.println(id+nom+prenom+fonction+date);
			}
		} catch (Exception e) {
			this.showErrorMessage("getList() ", className);
			e.printStackTrace();
		}
	
		return listePersonne;
	}

}
