package business;

public class Personne {
	
	private int idPersonne;
	private String nom;
	private String prenom;
	private String dateNaissance;
	private String fonction;
	private int idBadgeAssocie;
	
	/**
	 * Create a Personne from the attributes
	 * @param id
	 * @param nom2
	 * @param prenom2
	 * @param fonction2
	 * @param date
	 */
	
	public Personne(int id, String nom, String prenom, String fonction, String date) {
		this.idPersonne = id;
		this.nom = nom;
		this.prenom = prenom;
		this.fonction = fonction;
		this.dateNaissance = date;
	}
	/**
	 * @return the badgeAssocie
	 */
	public int getBadgeAssocie() {
		return idBadgeAssocie;
	}
	/**
	 * @param badgeAssocie the badgeAssocie to set
	 */
	public void setBadgeAssocie(int badgeAssocie) {
		this.idBadgeAssocie = badgeAssocie;
	}
	/**
	 * @return the idPersonne
	 */
	public int getIdPersonne() {
		return idPersonne;
	}
	/**
	 * @param idPersonne the idPersonne to set
	 */
	public void setIdPersonne(int idPersonne) {
		this.idPersonne = idPersonne;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * @return the dateNaissance
	 */
	public String getDateNaissance() {
		return dateNaissance;
	}
	/**
	 * @param dateNaissance the dateNaissance to set
	 */
	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	/**
	 * @return the fonction
	 */
	public String getFonction() {
		return fonction;
	}
	/**
	 * @param fonction the fonction to set
	 */
	public void setFonction(String fonction) {
		this.fonction = fonction;
	}
	
	
}
