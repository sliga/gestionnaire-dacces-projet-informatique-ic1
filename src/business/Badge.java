package business;

public class Badge {
	
	private int idBadge;
	private boolean etatBadgebloque;
	private int idPersonneAssocie;
	public Badge(int idBadge, int idPersonneAssocie) {
		this.idBadge = idBadge;
		this.idPersonneAssocie = idPersonneAssocie;
	}
	/**
	 * @return the idBadge
	 */
	public int getIdBadge() {
		return idBadge;
	}
	/**
	 * @param idBadge the idBadge to set
	 */
	public void setIdBadge(int idBadge) {
		this.idBadge = idBadge;
	}
	/**
	 * @return the etatBadgebloque
	 */
	public boolean isEtatBadgebloque() {
		return etatBadgebloque;
	}
	/**
	 * @param etatBadgebloque the etatBadgebloque to set
	 */
	public void setEtatBadgebloque(boolean etatBadgebloque) {
		this.etatBadgebloque = etatBadgebloque;
	}
	/**
	 * @return the personneAssocie
	 */
	public int getPersonneAssocie() {
		return idPersonneAssocie;
	}
	/**
	 * @param personneAssocie the personneAssocie to set
	 */
	public void setPersonneAssocie(int personneAssocie) {
		this.idPersonneAssocie = personneAssocie;
	}
	
	
}
