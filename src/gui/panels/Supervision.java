package gui.panels;

import javax.swing.JPanel;
import javax.swing.JDesktopPane;

public class Supervision extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the panel.
	 */
	public Supervision() {
		intitSupervision();
		createEvents();
	}

	/**
	 * Create and handle the events from the components
	 */
	private void createEvents() {

	}

	/**
	 * Create and initialize all the components of the SupervisionPanel
	 */
	private void intitSupervision() {
		setLayout(null);

		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBounds(0, 0, 1041, 668);
		add(desktopPane);
	}
}
