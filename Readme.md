# 		**Projet Ing�nieur 1 en Java :**
#		**MISE EN PLACE D�UN SYSTEME DE GESTION D�ACCES**

#### Voir les fichiers : 
*  __"[files/Sujets PDL 2018.pdf"][1]__ pour plus de d�tails sur le cahier de charges
*  __["files/DCP.pdf"][2]__ pour le ***Document de Conception pr�liminaires*** 
*  __["files/DSL.pdf"][3]__ pour le ***Document de Sp�cifications du Logiciel*** 
*  __["files/PVL.pdf"][4]__ pour le ***Plan de Validation du Logiciel*** 

[1]:https://bitbucket.org/sliga/gestionnaire-dacces-projet-informatique-ic1/src/master/files/Sujets%20PDL%202018.pdf
[2]:https://bitbucket.org/sliga/gestionnaire-dacces-projet-informatique-ic1/src/master/files/DCP.pdf
[3]:https://bitbucket.org/sliga/gestionnaire-dacces-projet-informatique-ic1/src/master/files/DSL.pdf
[4]:https://bitbucket.org/sliga/gestionnaire-dacces-projet-informatique-ic1/src/master/files/PVL.pdf

### *DESCRIPTION* 

*Dans le cadre de la mise en s�curit� de l�ISGE-BF, la CCI-BF, propri�taire des locaux, a lanc� un appel 
d�offres pour installer un syst�me de gestion d�acc�s. Le syst�me de gestion d�acc�s sera interfac� avec
notre annuaire d�authentification pour permettre une gestion personnalis�e des acc�s. Le syst�me
permettra de contr�ler et s�curiser les acc�s aux b�timents de l�ISGE-BF. En effet, il permet de mettre
en place des droits d�acc�s personnalis�s en fonction des zones du b�timent, du profil de l�utilisateur
et de l�horaire afin de v�rifier que seules les personnes autoris�es acc�dent � certaines zones. Toute
personne autoris�e � acc�der au site est dot�e ou sera dot�e d�une carte Bingo. Pour nous permettre
une gestion efficace et personnalis�e du site, nous vous proposons de contribuer � la mise en place
d�une application r�pondant aux attentes du service technique.*      

*Dans ce contexte, la direction des syst�mes d�information de l�ISGE-BF a d�cid� de lancer un concours
entre les �tudiants de l�ISGE-BF pour proposer une solution � cette probl�matique. Le client exige de
vous l�utilisation d�UML pour la documentation du travail.*


### *CAHIER DE CHARGES : BASES*

***L'application permet de :*** 
       
* __G�rer des personnes :__   
    * Cr�er une personne. Une personne est d�finie par un nom, un pr�nom, une date de naissance et d�une fonction.
    * Modifier les caract�ristiques d�une personne
    * Supprimer une personne    

* __G�rer les cartes Bingo : une carte Bingo sera d�finie par un badge dans notre application__
    * Cr�er un badge associ� � une personne
    * Supprimer un badge     

* __G�rer des lieux :__
    * Cr�er un lieu. Un lieu est d�fini par son emplacement, ses horaires d�ouverture et par son nombre d�acc�s
    * Modifier les caract�ristiques d�un lieu

### CAHIER DE CHARGES : GESTION D'ACCES

* __G�rer les acc�s � un lieu :__
    * Cr�er le type d�acc�s : porte, barri�re, parking, etc.
    * Cr�er un acc�s. Un acc�s � un type et est associ� � un lieu
    * Modifier les caract�ristiques d�un acc�s.
* __Lister les acc�s__
* __Rechercher un acc�s.__

### CAHIER DE CHARGES : GESTION DES PROFILS

* __G�rer des profils : un profil d�termine et donne des droits d�acc�s sp�cifiques aux personnes (cr�neaux horaires et lieux)__
    * Cr�er un profil
    * Associer un profil � une personne
    * Modifier/supprimer un profil
    * Changer le profil d�une personne
* __Lister les personnes par profil__
* __Rechercher une personne.__

### CAHIER DE CHARGES : GESTION ET SUPERVISION

* __Bloquer/d�bloquer un badge__
* __Bloquer/D�bloquer un lieu__
* __Effectuer un reporting � partir de l�historique de gestion__














