package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login extends JDialog implements ActionListener {

	/**
	 * Main global instance of the class
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textFieldUser;
	private JPasswordField textFieldPassword;
	private JButton btnCancel;
	private JButton btnLogin;

	/**
	 * Create the dialog by calling the method init().
	 */
	public Login() {
		init();
	}

	/**
	 * Create and initialize the components of the login dialog
	 */
	private void init() {
		btnLogin = new JButton("Login");
		btnCancel = new JButton("Cancel");

		setBackground(SystemColor.scrollbar);
		getContentPane().setBackground(SystemColor.activeCaptionBorder);
		setBounds(100, 100, 518, 361);
		getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Login.class.getResource("/resources/login.png")));
		lblNewLabel.setBounds(-29, 56, 211, 233);
		getContentPane().add(lblNewLabel);

		JLabel labelUser = new JLabel("Username");
		labelUser.setBackground(Color.LIGHT_GRAY);
		labelUser.setFont(new Font("Segoe UI Black", Font.BOLD, 18));
		labelUser.setBounds(166, 104, 107, 41);
		getContentPane().add(labelUser);

		textFieldUser = new JTextField();
		textFieldUser.setFont(new Font("Segoe UI Black", Font.PLAIN, 18));
		textFieldUser.setBounds(267, 107, 229, 34);
		getContentPane().add(textFieldUser);
		textFieldUser.setColumns(10);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Segoe UI Black", Font.BOLD, 18));
		lblPassword.setBackground(Color.LIGHT_GRAY);
		lblPassword.setBounds(166, 181, 114, 41);
		getContentPane().add(lblPassword);

		textFieldPassword = new JPasswordField();
		textFieldPassword.setFont(new Font("Segoe UI Black", Font.PLAIN, 18));
		textFieldPassword.setBounds(267, 184, 229, 34);
		getContentPane().add(textFieldPassword);

		btnLogin.addActionListener(this);

		btnLogin.setFont(new Font("Segoe UI", Font.PLAIN, 18));
		btnLogin.setBounds(200, 260, 114, 41);
		getContentPane().add(btnLogin);

		btnCancel.addActionListener(this);

		btnCancel.setFont(new Font("Segoe UI", Font.PLAIN, 18));
		btnCancel.setBounds(342, 260, 114, 41);
		getContentPane().add(btnCancel);

		JLabel lblPleaseLogOn = new JLabel("Please, log on to access the app");
		lblPleaseLogOn.setFont(new Font("Segoe UI", Font.ITALIC, 20));
		lblPleaseLogOn.setBounds(165, 46, 291, 34);
		getContentPane().add(lblPleaseLogOn);

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setUndecorated(true);
		setLocationRelativeTo(null);
		setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == btnCancel) {
			System.exit(0);
		} else if (event.getSource() == btnLogin) {
			if (checkLogin()) {
				try {
					UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
				} catch (Throwable e) {
					e.printStackTrace();
				}
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							Dashboard frame = new Dashboard();
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		}

	}

	private boolean checkLogin() {
		String user = "silga";
		String pass = "pass";

		boolean b = false;

		if (user.equals(textFieldUser.getText()) && pass.equals(textFieldPassword.getText())) {
			JOptionPane.showMessageDialog(null, "You are logged ! Please click on OK to pursue.");
			b = true;
		} else {
			JOptionPane.showMessageDialog(null, "Log on denied. Username or password invalid ", "Message d'erreur",
					JOptionPane.ERROR_MESSAGE);
		}
		return b;
	}

}
