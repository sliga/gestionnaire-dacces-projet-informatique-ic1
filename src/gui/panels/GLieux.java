package gui.panels;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;

import gui.events.ButtonAction;

public class GLieux extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_3;
	private JTable table_1;
	private JButton btnUpdate;
	private JButton btnCreate;
	private JButton btnDelete;
	private JButton btnSearch;

	/**
	 * Create the panel.
	 */
	public GLieux() {
		initGLieux();
		createEvents();
	}

	/**
	 * create events from the components
	 */
	private void createEvents() {
		ButtonAction.crudEvents(btnUpdate, btnCreate, btnDelete, btnSearch);

		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});

		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});

		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});

		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});

	}

	/**
	 * create and initialize the GLieux panel with all the components
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initGLieux() {
		setLayout(null);

		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBounds(0, 0, 849, 700);
		add(desktopPane);

		JLabel lblNom = new JLabel("Nom");
		lblNom.setBounds(399, 58, 63, 34);
		lblNom.setForeground(Color.WHITE);
		lblNom.setFont(new Font("Trebuchet MS", Font.BOLD, 22));

		JLabel label_1 = new JLabel("");
		label_1.setBounds(19, 106, 0, 179);

		JLabel lblNombreAccess = new JLabel("Nombre Access");
		lblNombreAccess.setBounds(308, 232, 168, 34);
		lblNombreAccess.setForeground(Color.WHITE);
		lblNombreAccess.setFont(new Font("Trebuchet MS", Font.BOLD, 22));

		JLabel lblEmplacement = new JLabel("Emplacement");
		lblEmplacement.setBounds(321, 92, 141, 34);
		lblEmplacement.setForeground(Color.WHITE);
		lblEmplacement.setFont(new Font("Trebuchet MS", Font.BOLD, 22));

		JLabel lblHeureFermeture = new JLabel("Heure Fermeture");
		lblHeureFermeture.setBounds(289, 186, 187, 34);
		lblHeureFermeture.setForeground(Color.WHITE);
		lblHeureFermeture.setFont(new Font("Trebuchet MS", Font.BOLD, 22));

		textField = new JTextField();
		textField.setBounds(483, 92, 278, 34);
		textField.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(483, 58, 278, 34);
		textField_1.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		textField_1.setColumns(10);

		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "07", "08", "09", "10", "11", "12", "13", "14", "15",
				"16", "17", "18", "19", "20", "21", "22", "23" }));
		comboBox.setBounds(483, 138, 75, 36);
		comboBox.setToolTipText("Hours");
		comboBox.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(
				new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "09", "10", "11", "12", "13", "14", "15",
						"16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31",
						"32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47",
						"48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
		comboBox_1.setBounds(576, 138, 76, 36);
		comboBox_1.setToolTipText("Minutes");
		comboBox_1.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		textField_3 = new JTextField();
		textField_3.setBounds(483, 232, 278, 34);
		textField_3.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		textField_3.setColumns(10);

		btnUpdate = new JButton("Update");
		btnUpdate.setIcon(new ImageIcon(GLieux.class.getResource("/resources/ok.png")));
		btnUpdate.setBounds(171, 335, 141, 44);
		btnUpdate.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		btnCreate = new JButton("Create");
		btnCreate.setIcon(new ImageIcon(GLieux.class.getResource("/resources/create.png")));
		btnCreate.setBounds(324, 335, 135, 44);
		btnCreate.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		btnDelete = new JButton("Delete");
		btnDelete.setIcon(new ImageIcon(GLieux.class.getResource("/resources/delete.png")));
		btnDelete.setBounds(471, 335, 135, 44);
		btnDelete.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		btnSearch = new JButton("Search");
		btnSearch.setIcon(new ImageIcon(GLieux.class.getResource("/resources/search.png")));
		btnSearch.setBounds(618, 335, 135, 44);
		btnSearch.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		table_1 = new JTable();
		table_1.setBounds(759, 498, 1, 1);

		JLabel lblHeureOuverture = new JLabel("Heure Ouverture");
		lblHeureOuverture.setBounds(289, 140, 187, 34);
		lblHeureOuverture.setForeground(Color.WHITE);
		lblHeureOuverture.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		desktopPane.setLayout(null);
		desktopPane.add(label_1);
		desktopPane.add(lblNombreAccess);
		desktopPane.add(lblHeureFermeture);
		desktopPane.add(lblNom);
		desktopPane.add(lblEmplacement);
		desktopPane.add(lblHeureOuverture);
		desktopPane.add(textField);
		desktopPane.add(textField_1);
		desktopPane.add(comboBox);
		desktopPane.add(comboBox_1);
		desktopPane.add(textField_3);
		desktopPane.add(btnUpdate);
		desktopPane.add(btnCreate);
		desktopPane.add(btnDelete);
		desktopPane.add(btnSearch);
		desktopPane.add(table_1);

		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] { "07", "08", "09", "10", "11", "12", "13", "14",
				"15", "16", "17", "18", "19", "20", "21", "22", "23" }));
		comboBox_2.setToolTipText("Hours");
		comboBox_2.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));
		comboBox_2.setBounds(483, 185, 75, 36);
		desktopPane.add(comboBox_2);

		JComboBox comboBox_3 = new JComboBox();
		comboBox_3.setModel(new DefaultComboBoxModel(
				new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "09", "10", "11", "12", "13", "14", "15",
						"16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31",
						"32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47",
						"48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
		comboBox_3.setToolTipText("Minutes");
		comboBox_3.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));
		comboBox_3.setBounds(576, 184, 76, 36);
		desktopPane.add(comboBox_3);

		JLabel lblEtat = new JLabel("Etat");
		lblEtat.setForeground(Color.WHITE);
		lblEtat.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		lblEtat.setBounds(413, 273, 63, 34);
		desktopPane.add(lblEtat);

		JRadioButton rdbtnBloqu = new JRadioButton("Bloqu\u00E9");
		rdbtnBloqu.setBackground(new Color(220, 20, 60));
		rdbtnBloqu.setForeground(new Color(0, 0, 0));
		rdbtnBloqu.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		rdbtnBloqu.setBounds(493, 273, 115, 34);
		desktopPane.add(rdbtnBloqu);

		JRadioButton rdbtnDbloqu = new JRadioButton("D\u00E9bloqu\u00E9");
		rdbtnDbloqu.setForeground(new Color(0, 0, 0));
		rdbtnDbloqu.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		rdbtnDbloqu.setBounds(620, 273, 141, 34);
		desktopPane.add(rdbtnDbloqu);

		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(GLieux.class.getResource("/resources/lieux_icon.png")));
		label.setBounds(81, 81, 156, 179);
		desktopPane.add(label);

	}
}
