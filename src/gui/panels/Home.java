package gui.panels;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Home extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel panelGP;
	private JPanel panelGB;
	private JPanel panelGL;
	private JPanel panelGA;
	private JPanel panelSup;
	private JPanel panelAdmin;
	private JTextArea txtTipsBadge;
	private JTextArea txtTipsPersonne;
	private JTextArea txtTipsLieux;
	private JTextArea txtTipsAccess;
	private JTextArea txtTipsSup;
	private JTextArea txtTipsAdmin;

	/**
	 * Create the panel.
	 */
	public Home() {

		initHome();
		createEvents();
	}

	private void createEvents() {
		panelGP.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				panelGP.setBackground(SystemColor.inactiveCaption);
				txtTipsPersonne.setBackground(SystemColor.inactiveCaption);

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				panelGP.setBackground(new Color(153, 153, 204));
				txtTipsPersonne.setBackground(new Color(153, 153, 204));
			}
		});

		panelGB.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				panelGB.setBackground(SystemColor.inactiveCaption);
				txtTipsBadge.setBackground(SystemColor.inactiveCaption);

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				panelGB.setBackground(new Color(153, 153, 204));
				txtTipsBadge.setBackground(new Color(153, 153, 204));
			}
		});

		panelGA.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				panelGA.setBackground(SystemColor.inactiveCaption);
				txtTipsAccess.setBackground(SystemColor.inactiveCaption);

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				panelGA.setBackground(new Color(153, 153, 204));
				txtTipsAccess.setBackground(new Color(153, 153, 204));
			}
		});

		panelSup.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				panelSup.setBackground(SystemColor.inactiveCaption);
				txtTipsSup.setBackground(SystemColor.inactiveCaption);

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				panelSup.setBackground(new Color(153, 153, 204));
				txtTipsSup.setBackground(new Color(153, 153, 204));
			}
		});

		panelAdmin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				panelAdmin.setBackground(SystemColor.inactiveCaption);
				txtTipsAdmin.setBackground(SystemColor.inactiveCaption);

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				panelAdmin.setBackground(new Color(153, 153, 204));
				txtTipsAdmin.setBackground(new Color(153, 153, 204));
			}
		});

		panelGB.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});

		panelGL.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});

		panelGL.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				panelGL.setBackground(SystemColor.inactiveCaption);
				txtTipsLieux.setBackground(SystemColor.inactiveCaption);

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				panelGL.setBackground(new Color(153, 153, 204));
				txtTipsLieux.setBackground(new Color(153, 153, 204));
			}
		});

	}

	private void initHome() {
		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBackground(new Color(204, 153, 255));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(desktopPane,
				GroupLayout.DEFAULT_SIZE, 811, Short.MAX_VALUE));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(desktopPane,
				GroupLayout.DEFAULT_SIZE, 491, Short.MAX_VALUE));

		panelGP = new JPanel();

		panelGP.setBackground(new Color(153, 153, 255));
		panelGP.setBorder(new EtchedBorder(EtchedBorder.RAISED, Color.BLACK, new Color(64, 64, 64)));
		panelGP.setBounds(27, 26, 400, 138);
		desktopPane.add(panelGP);
		panelGP.setLayout(null);

		JLabel labelIconPersonne = new JLabel("");
		labelIconPersonne.setIcon(new ImageIcon(Home.class.getResource("/resources/user_groupH.png")));
		labelIconPersonne.setBounds(6, 6, 100, 104);
		panelGP.add(labelIconPersonne);

		JLabel labelTitleGP = new JLabel("G\u00E9rer des Personnes");
		labelTitleGP.setFont(new Font("Tahoma", Font.BOLD, 26));
		labelTitleGP.setBounds(118, 6, 267, 43);
		panelGP.add(labelTitleGP);

		txtTipsPersonne = new JTextArea();
		txtTipsPersonne.setFont(new Font("Trebuchet MS", Font.PLAIN, 16));
		txtTipsPersonne.setLineWrap(true);
		txtTipsPersonne.setText(
				"Click here to get started managing\r\nPersons as updating, creating, \r\ndeleting searching and so many things.");
		txtTipsPersonne.setBackground(new Color(153, 153, 204));
		txtTipsPersonne.setEditable(false);
		txtTipsPersonne.setBounds(105, 49, 289, 86);
		panelGP.add(txtTipsPersonne);

		panelGB = new JPanel();

		panelGB.setBackground(new Color(255, 102, 153));
		panelGB.setBorder(new EtchedBorder(EtchedBorder.RAISED, Color.BLACK, Color.BLACK));
		panelGB.setBounds(483, 26, 410, 138);
		desktopPane.add(panelGB);
		panelGB.setLayout(null);

		JLabel labelIconBadge = new JLabel("");
		labelIconBadge.setIcon(new ImageIcon(Home.class.getResource("/resources/badgeH.png")));
		labelIconBadge.setBounds(6, 6, 100, 104);
		panelGB.add(labelIconBadge);

		JLabel labelTiltleGB = new JLabel("G\u00E9rer des Badges");
		labelTiltleGB.setFont(new Font("Tahoma", Font.BOLD, 26));
		labelTiltleGB.setBounds(127, 6, 267, 41);
		panelGB.add(labelTiltleGB);

		txtTipsBadge = new JTextArea();
		txtTipsBadge.setText(
				"Click here to get started managing  \r\nBagdes as updating, creating, deleting searching and so many things.");
		txtTipsBadge.setLineWrap(true);
		txtTipsBadge.setFont(new Font("Trebuchet MS", Font.PLAIN, 16));
		txtTipsBadge.setEditable(false);
		txtTipsBadge.setBackground(new Color(255, 102, 153));
		txtTipsBadge.setBounds(118, 49, 288, 86);
		panelGB.add(txtTipsBadge);

		panelGL = new JPanel();

		panelGL.setBackground(new Color(102, 0, 102));
		panelGL.setBorder(new EtchedBorder(EtchedBorder.RAISED, Color.BLACK, Color.BLACK));
		panelGL.setBounds(27, 193, 400, 138);
		desktopPane.add(panelGL);
		panelGL.setLayout(null);

		JLabel labelIconLieux = new JLabel("");
		labelIconLieux.setIcon(new ImageIcon(Home.class.getResource("/resources/lieuxH.png")));
		labelIconLieux.setBounds(6, 6, 100, 104);
		panelGL.add(labelIconLieux);

		JLabel labelTitleGL = new JLabel("G\u00E9rer des Lieux");
		labelTitleGL.setFont(new Font("Tahoma", Font.BOLD, 26));
		labelTitleGL.setBounds(118, 6, 267, 42);
		panelGL.add(labelTitleGL);

		txtTipsLieux = new JTextArea();
		txtTipsLieux.setText(
				"Click here to get started managing \r\nLieux as updating, creating, deleting \r\nsearching and so many things.");
		txtTipsLieux.setLineWrap(true);
		txtTipsLieux.setFont(new Font("Trebuchet MS", Font.PLAIN, 16));
		txtTipsLieux.setEditable(false);
		txtTipsLieux.setBackground(new Color(102, 0, 102));
		txtTipsLieux.setBounds(105, 49, 289, 86);
		panelGL.add(txtTipsLieux);

		panelGA = new JPanel();

		panelGA.setBackground(new Color(0, 153, 153));
		panelGA.setBorder(new EtchedBorder(EtchedBorder.RAISED, Color.BLACK, Color.BLACK));
		panelGA.setBounds(483, 193, 410, 138);
		desktopPane.add(panelGA);
		panelGA.setLayout(null);

		JLabel labelIconAccess = new JLabel("");
		labelIconAccess.setIcon(new ImageIcon(Home.class.getResource("/resources/lockerH.png")));
		labelIconAccess.setBounds(6, 6, 100, 104);
		panelGA.add(labelIconAccess);

		JLabel labelTitleGA = new JLabel("G\u00E9rer des Acc\u00E8s");
		labelTitleGA.setFont(new Font("Tahoma", Font.BOLD, 26));
		labelTitleGA.setBounds(127, 6, 267, 41);
		panelGA.add(labelTitleGA);

		txtTipsAccess = new JTextArea();
		txtTipsAccess.setText(
				"Click here to get started managing \r\nAccess as updating, creating, deleting \r\nsearching and so many things.");
		txtTipsAccess.setLineWrap(true);
		txtTipsAccess.setFont(new Font("Trebuchet MS", Font.PLAIN, 16));
		txtTipsAccess.setEditable(false);
		txtTipsAccess.setBackground(new Color(0, 153, 153));
		txtTipsAccess.setBounds(118, 48, 289, 86);
		panelGA.add(txtTipsAccess);

		panelSup = new JPanel();

		panelSup.setBackground(new Color(0, 102, 51));
		panelSup.setBorder(new EtchedBorder(EtchedBorder.RAISED, Color.BLACK, Color.BLACK));
		panelSup.setBounds(27, 363, 400, 138);
		desktopPane.add(panelSup);
		panelSup.setLayout(null);

		JLabel labelIconSup = new JLabel("");
		labelIconSup.setIcon(new ImageIcon(Home.class.getResource("/resources/supervisionH.png")));
		labelIconSup.setBounds(6, 6, 100, 104);
		panelSup.add(labelIconSup);

		JLabel labelTitleSup = new JLabel("Supervision\r\n");
		labelTitleSup.setFont(new Font("Tahoma", Font.BOLD, 26));
		labelTitleSup.setBounds(118, 6, 267, 40);
		panelSup.add(labelTitleSup);

		txtTipsSup = new JTextArea();
		txtTipsSup.setText(
				"Click here to get started monitoring\r\nof the system in the whole. Get here  information and statistics about the\r\nsite, its users and control it.");
		txtTipsSup.setLineWrap(true);
		txtTipsSup.setFont(new Font("Trebuchet MS", Font.PLAIN, 16));
		txtTipsSup.setEditable(false);
		txtTipsSup.setBackground(new Color(0, 102, 51));
		txtTipsSup.setBounds(105, 52, 289, 86);
		panelSup.add(txtTipsSup);

		panelAdmin = new JPanel();
		panelAdmin.setBackground(new Color(204, 0, 51));
		panelAdmin.setBorder(new EtchedBorder(EtchedBorder.RAISED, Color.BLACK, Color.BLACK));
		panelAdmin.setBounds(483, 363, 410, 138);
		desktopPane.add(panelAdmin);
		panelAdmin.setLayout(null);

		JLabel labelIconAdmin = new JLabel("");
		labelIconAdmin.setIcon(new ImageIcon(Home.class.getResource("/resources/adminH.png")));
		labelIconAdmin.setBounds(6, 6, 100, 104);
		panelAdmin.add(labelIconAdmin);

		JLabel labelTitleAdmin = new JLabel("Administration\r\n");
		labelTitleAdmin.setFont(new Font("Tahoma", Font.BOLD, 26));
		labelTitleAdmin.setBounds(127, 6, 267, 41);
		panelAdmin.add(labelTitleAdmin);

		txtTipsAdmin = new JTextArea();
		txtTipsAdmin.setText(
				"Click here to get started managing \r\n users of the app and administration of the app or so many things.");
		txtTipsAdmin.setLineWrap(true);
		txtTipsAdmin.setFont(new Font("Trebuchet MS", Font.PLAIN, 16));
		txtTipsAdmin.setEditable(false);
		txtTipsAdmin.setBackground(new Color(204, 0, 51));
		txtTipsAdmin.setBounds(118, 48, 289, 86);
		panelAdmin.add(txtTipsAdmin);
		setLayout(groupLayout);

	}
}
