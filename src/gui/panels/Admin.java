package gui.panels;

import javax.swing.JDesktopPane;
import javax.swing.JPanel;

public class Admin extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the panel.
	 */
	public Admin() {
		initAdmin();
		createEvents();
	}

	/**
	 * Create and handle the events from the components
	 */
	private void createEvents() {

	}

	/**
	 * Create and initialize all the components of the AdminPanel
	 */
	private void initAdmin() {
		setLayout(null);

		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBounds(0, 0, 1009, 698);
		add(desktopPane);

	}

}
