package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import gui.panels.Admin;
import gui.panels.GAccess;
import gui.panels.GBadge;
import gui.panels.GLieux;
import gui.panels.GPersonne;
import gui.panels.Home;
import gui.panels.Supervision;
import javax.swing.event.MenuListener;
import javax.swing.event.MenuEvent;

public class Dashboard extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	private JPanel home;
	private JPanel gpersonne;
	private JPanel gbadge;
	private JPanel gaccess;
	private JPanel glieux;
	private JPanel supervision;
	private JPanel admin;

	private JPanel dynamicPanel;
	private GridBagLayout layout;

	private JPanel panelHome;
	private JPanel panelGP;
	private JPanel panelGB;
	private JPanel panelGL;
	private JPanel panelGA;
	private JPanel panelAdmin;
	private JPanel panelSup;
	private JLabel labelDateSetting;
	private JLabel labelLoggedAs;
	private JLabel lblLogOut;
	private JPanel panelLogOut;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dashboard frame = new Dashboard();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Dashboard() {
		initDashboard();
		initPanels();
		createEvents();
	}

	public String setDispalyedUserLogged() {
		String username = "silga";
		return username;
	}

	/**
	 * Get the current date to display on the dashboard
	 */

	@SuppressWarnings("finally")
	public String setDisplayedDate() {
		DateFormat df = new SimpleDateFormat("dd/MM/yy");
		Calendar date = Calendar.getInstance();
		String dateF = df.format(date.getTime());
		String d = "";
		try {
			date.setTime(df.parse(dateF));
			d = "" + date.getTime();
		} catch (ParseException e) {
			System.out.println("ERROR for the date setting");
		} finally {
			return d;
		}
	}

	/**
	 * Create and handle the events form the components MouseListener used
	 */
	private void createEvents() {
		panelHome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				home.setVisible(true);
				gpersonne.setVisible(false);
				gbadge.setVisible(false);
				glieux.setVisible(false);
				gaccess.setVisible(false);
				supervision.setVisible(false);
				admin.setVisible(false);
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				panelHome.setBackground(SystemColor.activeCaption);
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				panelHome.setBackground(SystemColor.windowBorder);
			}
		});

		panelGP.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				home.setVisible(false);
				gpersonne.setVisible(true);
				gbadge.setVisible(false);
				glieux.setVisible(false);
				gaccess.setVisible(false);
				supervision.setVisible(false);
				admin.setVisible(false);
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				panelGP.setBackground(SystemColor.activeCaption);
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				panelGP.setBackground(SystemColor.windowBorder);
			}
		});

		panelGB.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				home.setVisible(false);
				gpersonne.setVisible(false);
				gbadge.setVisible(true);
				glieux.setVisible(false);
				gaccess.setVisible(false);
				supervision.setVisible(false);
				admin.setVisible(false);
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				panelGB.setBackground(SystemColor.activeCaption);
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				panelGB.setBackground(SystemColor.windowBorder);
			}
		});

		panelGL.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				home.setVisible(false);
				gpersonne.setVisible(false);
				gbadge.setVisible(false);
				glieux.setVisible(true);
				gaccess.setVisible(false);
				supervision.setVisible(false);
				admin.setVisible(false);
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				panelGL.setBackground(SystemColor.activeCaption);
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				panelGL.setBackground(SystemColor.windowBorder);
			}
		});

		panelGA.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				home.setVisible(false);
				gpersonne.setVisible(false);
				gbadge.setVisible(false);
				glieux.setVisible(false);
				gaccess.setVisible(true);
				supervision.setVisible(false);
				admin.setVisible(false);
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				panelGA.setBackground(SystemColor.activeCaption);
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				panelGA.setBackground(SystemColor.windowBorder);
			}
		});

		panelSup.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				home.setVisible(false);
				gpersonne.setVisible(false);
				gbadge.setVisible(false);
				glieux.setVisible(false);
				gaccess.setVisible(false);
				supervision.setVisible(true);
				admin.setVisible(false);
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				panelSup.setBackground(SystemColor.activeCaption);
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				panelSup.setBackground(SystemColor.windowBorder);
			}
		});

		panelAdmin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				home.setVisible(false);
				gpersonne.setVisible(false);
				gbadge.setVisible(false);
				glieux.setVisible(false);
				gaccess.setVisible(false);
				supervision.setVisible(false);
				admin.setVisible(true);
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				panelAdmin.setBackground(SystemColor.activeCaption);
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				panelAdmin.setBackground(SystemColor.windowBorder);
			}
		});

		panelLogOut.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Dashboard.this.setVisible(false);
				try {
					Login dialog = new Login();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				panelLogOut.setBackground(SystemColor.activeCaption);
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				panelLogOut.setBackground(new Color(214, 217, 223));
			}
		});

	}

	private void initPanels() {
		home = new Home();
		gpersonne = new GPersonne();
		gaccess = new GAccess();
		glieux = new GLieux();
		gbadge = new GBadge();
		admin = new Admin();
		supervision = new Supervision();

		layout = new GridBagLayout();
		layout.columnWeights = new double[] { 1.0 };
		layout.rowWeights = new double[] { 1.0 };

		dynamicPanel.setLayout(layout);

		GridBagConstraints gbc_home = new GridBagConstraints();
		gbc_home.fill = GridBagConstraints.BOTH;
		gbc_home.insets = new Insets(0, 0, 5, 0);
		gbc_home.gridx = 0;
		gbc_home.gridy = 0;

		GridBagConstraints gbc_gpersonne = new GridBagConstraints();
		gbc_gpersonne.fill = GridBagConstraints.VERTICAL;
		gbc_gpersonne.insets = new Insets(0, 0, 5, 0);
		gbc_gpersonne.gridx = 0;
		gbc_gpersonne.gridy = 0;

		GridBagConstraints gbc_gbadge = new GridBagConstraints();
		gbc_gbadge.fill = GridBagConstraints.BOTH;
		gbc_gbadge.insets = new Insets(0, 0, 5, 0);
		gbc_gbadge.gridx = 0;
		gbc_gbadge.gridy = 0;

		GridBagConstraints gbc_glieux = new GridBagConstraints();
		gbc_glieux.fill = GridBagConstraints.BOTH;
		gbc_glieux.insets = new Insets(0, 0, 5, 0);
		gbc_glieux.gridx = 0;
		gbc_glieux.gridy = 0;

		GridBagConstraints gbc_gaccess = new GridBagConstraints();
		gbc_gaccess.fill = GridBagConstraints.BOTH;
		gbc_gaccess.insets = new Insets(0, 0, 5, 0);
		gbc_gaccess.gridx = 0;
		gbc_gaccess.gridy = 0;

		GridBagConstraints gbc_sup = new GridBagConstraints();
		gbc_sup.fill = GridBagConstraints.BOTH;
		gbc_sup.insets = new Insets(0, 0, 5, 0);
		gbc_sup.gridx = 0;
		gbc_sup.gridy = 0;

		GridBagConstraints gbc_admin = new GridBagConstraints();
		gbc_admin.fill = GridBagConstraints.BOTH;
		gbc_admin.insets = new Insets(0, 0, 5, 0);
		gbc_admin.gridx = 0;
		gbc_admin.gridy = 0;

		dynamicPanel.add(home, gbc_home);
		dynamicPanel.add(gpersonne, gbc_gpersonne);
		dynamicPanel.add(gbadge, gbc_gbadge);
		dynamicPanel.add(glieux, gbc_glieux);
		dynamicPanel.add(gaccess, gbc_gaccess);
		dynamicPanel.add(supervision, gbc_sup);
		dynamicPanel.add(admin, gbc_admin);

		home.setVisible(true);
		gpersonne.setVisible(false);
		gbadge.setVisible(false);
		glieux.setVisible(false);
		gaccess.setVisible(false);
		supervision.setVisible(false);
		admin.setVisible(false);
	}

	private void initDashboard() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Dashboard.class.getResource("/resources/logo.png")));
		setFont(new Font("Tahoma", Font.BOLD, 16));
		setTitle("AccessMag");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setBounds(100, 100, 1231, 750);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(10, 5, 5, 10));
		setContentPane(contentPane);

		JPanel panelDash = new JPanel();
		panelDash.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		panelDash.setBackground(SystemColor.windowBorder);

		JPanel panel = new JPanel();

		dynamicPanel = new JPanel();
		dynamicPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));

		JPanel panelTitle = new JPanel();
		panelTitle.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelTitle.setBackground(SystemColor.windowBorder);

		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);

		labelLoggedAs = new JLabel(this.setDispalyedUserLogged());
		labelLoggedAs.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		labelLoggedAs.setBounds(119, 0, 153, 36);
		panel_1.add(labelLoggedAs);

		JLabel lblLoggedAs = new JLabel("Logged as ");
		lblLoggedAs.setIcon(new ImageIcon(Dashboard.class.getResource("/resources/user_logged.png")));
		lblLoggedAs.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblLoggedAs.setBounds(6, 0, 125, 36);
		panel_1.add(lblLoggedAs);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane
				.createSequentialGroup().addContainerGap()
				.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(panelDash, GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE).addGap(18)
								.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
										.addGroup(gl_contentPane.createSequentialGroup()
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(dynamicPanel, GroupLayout.DEFAULT_SIZE, 937,
														Short.MAX_VALUE)
												.addGap(14))
										.addGroup(gl_contentPane.createSequentialGroup().addGap(9).addComponent(
												panelTitle, GroupLayout.DEFAULT_SIZE, 942, Short.MAX_VALUE))
										.addGroup(gl_contentPane.createSequentialGroup()
												.addComponent(panel, GroupLayout.PREFERRED_SIZE, 680,
														GroupLayout.PREFERRED_SIZE)
												.addGap(6))))
						.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 490, GroupLayout.PREFERRED_SIZE)
								.addContainerGap(704, Short.MAX_VALUE)))));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createSequentialGroup().addGap(6).addComponent(panel,
										GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)))
				.addPreferredGap(ComponentPlacement.RELATED)
				.addGroup(
						gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_contentPane.createSequentialGroup()
										.addComponent(panelTitle, GroupLayout.PREFERRED_SIZE, 39,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(dynamicPanel, GroupLayout.DEFAULT_SIZE, 641, Short.MAX_VALUE))
						.addComponent(panelDash, GroupLayout.PREFERRED_SIZE, 686, GroupLayout.PREFERRED_SIZE))
				.addGap(0)));

		panelLogOut = new JPanel();
		panelLogOut.setToolTipText("Click to log out");
		panelLogOut.setBounds(335, 0, 120, 42);
		panel_1.add(panelLogOut);

		lblLogOut = new JLabel("Log out");
		panelLogOut.add(lblLogOut);
		lblLogOut.setBackground(new Color(250, 128, 114));
		lblLogOut.setIcon(new ImageIcon(Dashboard.class.getResource("/resources/exit.png")));
		lblLogOut.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel.setLayout(null);

		JMenu mnNewMenu = new JMenu("Help");
		mnNewMenu.setIcon(new ImageIcon(Dashboard.class.getResource("/resources/help.png")));
		mnNewMenu.setFont(new Font("Tahoma", Font.PLAIN, 16));
		mnNewMenu.setBounds(543, 0, 111, 36);
		panel.add(mnNewMenu);

		JMenuItem mntmNewMenuItem = new JMenuItem("About AccessMag");
		mntmNewMenuItem.setFont(new Font("Tahoma", Font.PLAIN, 16));
		mntmNewMenuItem.setSelected(true);
		mnNewMenu.add(mntmNewMenuItem);

		JMenuItem mntmHome = new JMenuItem("Home");
		mntmHome.setSelected(true);
		mntmHome.setFont(new Font("Tahoma", Font.PLAIN, 16));
		mnNewMenu.add(mntmHome);

		labelDateSetting = new JLabel(this.setDisplayedDate());
		labelDateSetting.setFont(new Font("Tahoma", Font.PLAIN, 16));
		labelDateSetting.setBounds(92, 0, 408, 36);
		panel.add(labelDateSetting);

		JLabel lblNewLabel = new JLabel("Date");
		lblNewLabel.setBounds(6, 0, 74, 36);
		panel.add(lblNewLabel);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setIcon(new ImageIcon(Dashboard.class.getResource("/resources/calendar.png")));
		panelDash.setLayout(null);

		JLabel labelDashboard = new JLabel("Dashboard");
		labelDashboard.setFont(new Font("Tahoma", Font.PLAIN, 36));
		labelDashboard.setForeground(SystemColor.textHighlightText);
		labelDashboard.setBounds(18, 6, 177, 65);
		panelDash.add(labelDashboard);

		panelHome = new JPanel();
		panelHome.setBackground(SystemColor.windowBorder);
		panelHome.setBounds(6, 85, 213, 55);
		panelDash.add(panelHome);
		panelHome.setLayout(null);

		JLabel labelHomeIcon = new JLabel("");
		labelHomeIcon.setIcon(new ImageIcon(Dashboard.class.getResource("/resources/home.png")));
		labelHomeIcon.setBounds(6, 6, 32, 43);
		panelHome.add(labelHomeIcon);

		JLabel lblHome = new JLabel("Home");
		lblHome.setForeground(SystemColor.textHighlightText);
		lblHome.setFont(new Font("Trebuchet MS", Font.PLAIN, 18));
		lblHome.setBounds(50, 6, 81, 43);
		panelHome.add(lblHome);

		panelGP = new JPanel();
		panelGP.setBackground(SystemColor.windowBorder);
		panelGP.setBounds(6, 160, 213, 55);
		panelDash.add(panelGP);
		panelGP.setLayout(null);

		JLabel labelGPIcon = new JLabel("");
		labelGPIcon.setIcon(new ImageIcon(Dashboard.class.getResource("/resources/group_user.png")));
		labelGPIcon.setBounds(6, 6, 38, 43);
		panelGP.add(labelGPIcon);

		JLabel lblGrerDesPersonnes = new JLabel("G\u00E9rer des Personnes");
		lblGrerDesPersonnes.setForeground(Color.WHITE);
		lblGrerDesPersonnes.setFont(new Font("Trebuchet MS", Font.PLAIN, 18));
		lblGrerDesPersonnes.setBounds(47, 6, 182, 43);
		panelGP.add(lblGrerDesPersonnes);

		panelGB = new JPanel();
		panelGB.setBackground(SystemColor.windowBorder);
		panelGB.setBounds(6, 238, 213, 55);
		panelDash.add(panelGB);
		panelGB.setLayout(null);

		JLabel labelGBIcon = new JLabel("");
		labelGBIcon.setIcon(new ImageIcon(Dashboard.class.getResource("/resources/badge.png")));
		labelGBIcon.setBounds(6, 6, 38, 43);
		panelGB.add(labelGBIcon);

		JLabel lblGrerDesBadges = new JLabel("G\u00E9rer des Badges");
		lblGrerDesBadges.setForeground(Color.WHITE);
		lblGrerDesBadges.setFont(new Font("Trebuchet MS", Font.PLAIN, 18));
		lblGrerDesBadges.setBounds(48, 6, 182, 43);
		panelGB.add(lblGrerDesBadges);

		panelGL = new JPanel();
		panelGL.setBackground(SystemColor.windowBorder);
		panelGL.setBounds(6, 314, 219, 55);
		panelDash.add(panelGL);
		panelGL.setLayout(null);

		JLabel labelGLIcon = new JLabel("");
		labelGLIcon.setIcon(new ImageIcon(Dashboard.class.getResource("/resources/lieu.png")));
		labelGLIcon.setBounds(6, 0, 38, 43);
		panelGL.add(labelGLIcon);

		JLabel lblGrerDesLieux = new JLabel("G\u00E9rer des Lieux");
		lblGrerDesLieux.setForeground(Color.WHITE);
		lblGrerDesLieux.setFont(new Font("Trebuchet MS", Font.PLAIN, 18));
		lblGrerDesLieux.setBounds(48, 0, 182, 43);
		panelGL.add(lblGrerDesLieux);

		panelGA = new JPanel();
		panelGA.setBackground(SystemColor.windowBorder);
		panelGA.setBounds(6, 391, 219, 47);
		panelDash.add(panelGA);
		panelGA.setLayout(null);

		JLabel labelGAIcon = new JLabel("");
		labelGAIcon.setIcon(new ImageIcon(Dashboard.class.getResource("/resources/locker.png")));
		labelGAIcon.setBounds(6, 6, 38, 43);
		panelGA.add(labelGAIcon);

		JLabel lblGrerDesAccs = new JLabel("G\u00E9rer des Acc\u00E8s");
		lblGrerDesAccs.setForeground(Color.WHITE);
		lblGrerDesAccs.setFont(new Font("Trebuchet MS", Font.PLAIN, 18));
		lblGrerDesAccs.setBounds(48, 6, 182, 43);
		panelGA.add(lblGrerDesAccs);

		panelAdmin = new JPanel();
		panelAdmin.setBackground(SystemColor.windowBorder);
		panelAdmin.setBounds(6, 545, 219, 55);
		panelDash.add(panelAdmin);
		panelAdmin.setLayout(null);

		JLabel labelAdminIcon = new JLabel("");
		labelAdminIcon.setIcon(new ImageIcon(Dashboard.class.getResource("/resources/admin.png")));
		labelAdminIcon.setBounds(6, 6, 38, 43);
		panelAdmin.add(labelAdminIcon);

		JLabel lblAdministration = new JLabel("Administration");
		lblAdministration.setForeground(Color.WHITE);
		lblAdministration.setFont(new Font("Trebuchet MS", Font.PLAIN, 18));
		lblAdministration.setBounds(47, 6, 152, 43);
		panelAdmin.add(lblAdministration);

		panelSup = new JPanel();
		panelSup.setBackground(SystemColor.windowBorder);
		panelSup.setBounds(6, 468, 219, 55);
		panelDash.add(panelSup);
		panelSup.setLayout(null);

		JLabel labelSup = new JLabel("");
		labelSup.setIcon(new ImageIcon(Dashboard.class.getResource("/resources/supervision.png")));
		labelSup.setBounds(6, 6, 38, 43);
		panelSup.add(labelSup);

		JLabel lblSupervision = new JLabel("Supervision");
		lblSupervision.setForeground(Color.WHITE);
		lblSupervision.setFont(new Font("Trebuchet MS", Font.PLAIN, 18));
		lblSupervision.setBounds(45, 6, 152, 43);
		panelSup.add(lblSupervision);
		contentPane.setLayout(gl_contentPane);
	}
}
