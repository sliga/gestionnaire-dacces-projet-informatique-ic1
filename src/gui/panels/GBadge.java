package gui.panels;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableModel;

import business.Access;
import business.Badge;
import dao.BadgeDAO;
import gui.events.ButtonAction;
import gui.panels.GAccess.ComboItemState;

import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.Dimension;
import java.awt.Component;

public class GBadge extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textFieldCarteBingo;
	private JTextField textFieldRefBadge;
	private JTextField textFieldPersonneAssocie;
	private JButton btnUpdate;
	private JButton btnCreate;
	private JButton btnDelete;
	private JButton btnSearch;
	private TableModel tableModel;
	private String tableHeading[] = { "R�f. Badge", "Carte bingo", "Id Personne associ�e" };
	private JTable table;

	private int id_badge;
	private int id_personne_associe;
	private Badge badge;
	private BadgeDAO bd;

	/**
	 * Create the panel.
	 */
	public GBadge() {
		bd = new BadgeDAO();
		initGBadge();
		createEvents();
	}

	/**
	 * Read the input values from the input components as textField...
	 */
	public void inputValues() {
		id_badge = Integer.parseInt(textFieldRefBadge.getText());
		id_personne_associe = Integer.parseInt(textFieldPersonneAssocie.getText());
		badge = new Badge(id_badge, id_personne_associe);
	}

	/**
	 * Create events from the components
	 */
	private void createEvents() {
		ButtonAction.crudEvents(btnUpdate, btnCreate, btnDelete, btnSearch);

		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// BadgeDAO bd = new BadgeDAO();
				// Badge b = new Badge(11, 6);
				inputValues();
				bd.update(badge);
			}
		});

		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inputValues();
				bd.create(badge);
			}
		});

		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inputValues();
				bd.delete(badge);
			}
		});

		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inputValues();
				bd.find(badge.getIdBadge());
			}
		});
	}

	/**
	 * Create and initialize the GBadge panel with all the components
	 */
	private void initGBadge() {
		setLayout(null);
		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBounds(6, 6, 875, 764);
		add(desktopPane);

		JLabel lblBadgeRef = new JLabel("R\u00E9f. Badge");
		lblBadgeRef.setBounds(401, 58, 131, 34);
		lblBadgeRef.setForeground(Color.WHITE);
		lblBadgeRef.setFont(new Font("Trebuchet MS", Font.BOLD, 22));

		JLabel lblIdPersonneAssocie = new JLabel("ID Personne associ\u00E9e");
		lblIdPersonneAssocie.setBounds(317, 150, 215, 34);
		lblIdPersonneAssocie.setForeground(Color.WHITE);
		lblIdPersonneAssocie.setFont(new Font("Trebuchet MS", Font.BOLD, 22));

		JLabel lblCarteBingo = new JLabel("Carte Bingo");
		lblCarteBingo.setBounds(413, 104, 119, 34);
		lblCarteBingo.setForeground(Color.WHITE);
		lblCarteBingo.setFont(new Font("Trebuchet MS", Font.BOLD, 22));

		textFieldCarteBingo = new JTextField();
		textFieldCarteBingo.setBounds(542, 104, 278, 34);
		textFieldCarteBingo.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		textFieldCarteBingo.setColumns(10);

		textFieldRefBadge = new JTextField();
		textFieldRefBadge.setBounds(542, 58, 278, 34);
		textFieldRefBadge.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		textFieldRefBadge.setColumns(10);

		btnUpdate = new JButton("Update");
		btnUpdate.setBounds(200, 239, 143, 44);
		btnUpdate.setIcon(new ImageIcon(GBadge.class.getResource("/resources/ok.png")));
		btnUpdate.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		btnCreate = new JButton("Create");
		btnCreate.setBounds(371, 239, 135, 44);
		btnCreate.setIcon(new ImageIcon(GBadge.class.getResource("/resources/create.png")));
		btnCreate.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		btnDelete = new JButton("Delete");
		btnDelete.setBounds(528, 239, 135, 44);
		btnDelete.setIcon(new ImageIcon(GBadge.class.getResource("/resources/delete.png")));
		btnDelete.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		btnSearch = new JButton("Search");
		btnSearch.setBounds(685, 239, 135, 44);
		btnSearch.setIcon(new ImageIcon(GBadge.class.getResource("/resources/search.png")));
		btnSearch.setFont(new Font("Trebuchet MS", Font.PLAIN, 22));

		textFieldPersonneAssocie = new JTextField();
		textFieldPersonneAssocie.setBounds(542, 150, 278, 34);
		textFieldPersonneAssocie.setFont(new Font("Trebuchet MS", Font.BOLD, 22));
		textFieldPersonneAssocie.setColumns(10);
		desktopPane.setLayout(null);

		JLabel labelIconBadge = new JLabel("");
		labelIconBadge.setBounds(143, 49, 156, 179);
		labelIconBadge.setIcon(new ImageIcon(GBadge.class.getResource("/resources/badge_icon.png")));
		desktopPane.add(labelIconBadge);
		desktopPane.add(lblIdPersonneAssocie);
		desktopPane.add(lblCarteBingo);
		desktopPane.add(lblBadgeRef);
		desktopPane.add(textFieldCarteBingo);
		desktopPane.add(textFieldRefBadge);
		desktopPane.add(textFieldPersonneAssocie);
		desktopPane.add(btnUpdate);
		desktopPane.add(btnCreate);
		desktopPane.add(btnDelete);
		desktopPane.add(btnSearch);

		Object data[][] = { { "12", "123213", "1" }, { "45", "1112255", "7" }, { "45", "1112255", "7" },
				{ "45", "1112255", "7" }, { "45", "1112255", "7" }, { "45", "1112255", "7" }, { "45", "1112255", "7" },
				{ "45", "1112255", "7" }, { "45", "1112255", "7" } };
		JScrollPane tableSrcollTable = new JScrollPane();
		tableSrcollTable.setFont(new Font("Tahoma", Font.BOLD, 26));
		tableSrcollTable.setViewportBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		desktopPane.setLayer(tableSrcollTable, 0);
		tableSrcollTable.setSize(738, 239);
		tableSrcollTable.setLocation(72, 315);
		desktopPane.add(tableSrcollTable);

		table = new JTable();
		table.setColumnSelectionAllowed(true);
		table.setCellSelectionEnabled(true);
		table.setName("Liste des Badges");
		table.setRowHeight(30);
		table.setSurrendersFocusOnKeystroke(true);
		table.setShowVerticalLines(true);
		table.setShowHorizontalLines(true);
		table.setFont(new Font("Tahoma", Font.PLAIN, 20));
		table.setModel(new DefaultTableModel(data, tableHeading));
		tableSrcollTable.setViewportView(table);

	}
}
