package gui.events;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class ButtonAction {
	/**
	 * Handle the CRUD button from the panel
	 * @param btnUpdate
	 * @param btnCreate
	 * @param btnDelete
	 * @param btnSearch
	 */
	public static void crudEvents(JButton btnUpdate,JButton btnCreate, JButton btnDelete,JButton btnSearch){
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});

		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});

		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});

		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
	}

}
