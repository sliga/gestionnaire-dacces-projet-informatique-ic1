/**
 * 
 */
package dao;

import java.sql.SQLWarning;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;
import javax.swing.JOptionPane;

/**
 * @author ASUS
 *
 */
public abstract class Dao<O> {
	/**
	 * @param URL is final static and is used for the url connection
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1593:xe";
	/**
	 * @param LOGIN is final static and is used for the username connection
	 */
	final static String LOGIN = "silga";
	/**
	 * @param URL is final static and is used for the password connection
	 */
	final static String PASS = "password";
	/**
	 * @param sql is static and is used for the sql commands
	 */
	protected static String sql; 
	/**
	 * @param object is used to substitute any Business object in DAO layout
	 */
	protected O object;
	/**
	 * @param executeStatus is used to get the execute status
	 */
	protected boolean executeStatus;
	
	public boolean isExecuteStatus() {
		return executeStatus;
	}

	/**
	 * @param warning is used to get QSLWarning about the SQL operation done.
	 */
	protected SQLWarning warning;
	/**
	 * @param message is used to get message about the SQL operation done.
	 */
	protected String message;
	/**
	 * @param id is used to identify unique way an element in the database
	 */
	protected int id;
	
	
	/**
	 * Allows to get some thing existed in Data base from its id
	 * @param id
	 * @return
	 */
	public abstract O find (int id);
	
	/**
	 * Allows to add some thing in data base according the object nature
	 * @param object
	 */
	public abstract boolean create(O object);
	
	/**
	 * Allows an update of an element of data base
	 * @param object
	 */
	public abstract boolean update(O object);
	
	/**
	 * Allows to delete permanently an element of data base
	 * @param object
	 */
	public abstract boolean delete(O object);
	
	/**
	 * Allows to get a list of all some kind elements from the data base
	 * @param object
	 */
	public abstract List<O> getList();
	
	
	/**
	 * Allows to set auto-increment the id value in table into the database;
	 * @param tableName
	 */
	public int autoIncrementId(String tableName) {
		int id = 0;
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			JdbcRowSet rs = rsFact.createJdbcRowSet();
			sql= "SELECT COUNT (*) AS Rowcount FROM "+tableName;
			
			rs= Dao.getRowSet();
			rs.setCommand(sql);
			rs.execute();
			while(rs.next()){
				id = rs.getInt("Rowcount");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}
	
	/**
	 * Gives a JdbcRowSet connected with the connection parameters
	 * and return a one.
	 * @return
	 */
	public static JdbcRowSet getRowSet(){
		JdbcRowSet rs =null;
		try {
			RowSetFactory rsFact = RowSetProvider.newFactory();
			rs = rsFact.createJdbcRowSet();
			rs.setUrl(URL);
			rs.setUsername(LOGIN);
			rs.setPassword(PASS);
		} catch (Exception e) {
			System.out.println("Impossible d'acc�der aux infos");
			e.printStackTrace();
		}
		return rs;	
	}
	
	/**
	 * Shows an error message using a JOptionPane for the errors due to sql execution
	 * @param method
	 * @param className
	 */
	public void showErrorMessage(String method, String className){
		message = String.format("%s.%s : Impossible d'acc�der aux infos\n%s",className,method,"Check your input values");
		JOptionPane.showMessageDialog(null, message, "Error Message", JOptionPane.ERROR_MESSAGE);
		System.out.println(className+"." + ":Impossible d'acc�der aux infos");
	}
	
	public void showValidExectutionMessage(String method, String element){
		element = element.toUpperCase();
		switch (method) {
		case "update":
			message = String.format("%s updated", element);
			break;
		case "create":
			message = String.format("%s added", element);
			break;
		case "delete":
			message = String.format("%s deleted", element);
			break;
		default:
			break;
		}
		JOptionPane.showMessageDialog(null, message, "Operation succeeded", JOptionPane.INFORMATION_MESSAGE);
	}

}
